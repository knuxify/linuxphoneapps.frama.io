+++
title = "Monophony"
description = "Stream music from YouTube"
aliases = []
date = 2023-02-19
updated = 2023-09-30

[taxonomies]
project_licenses = [ "AGPL-3.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "zehkira",]
categories = [ "music player",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "yt-dlp", "ytmusicapi",]
services = [ "YouTube",]
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "Audio", "Network", "Player",]
programming_languages = [ "Python",]
build_systems = [ "make",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/zehkira/monophony"
homepage = "https://gitlab.com/zehkira/monophony"
bugtracker = "https://gitlab.com/zehkira/monophony/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/zehkira/monophony/-/raw/master/source/data/metainfo.xml"
screenshots = [ "https://gitlab.com/zehkira/monophony/-/raw/master/assets/screenshot1.png", "https://gitlab.com/zehkira/monophony/-/raw/master/assets/screenshot2.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/io.gitlab.zehkira.monophony/1.png", "https://img.linuxphoneapps.org/io.gitlab.zehkira.monophony/2.png",]
all_features_touch = true
intended_for_mobile = false
app_id = "io.gitlab.zehkira.Monophony"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.gitlab.zehkira.Monophony"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "monophony",]
appstream_xml_url = "https://gitlab.com/zehkira/monophony/-/raw/master/source/data/metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++

### Description
Listen to your favorite music without ads. Create local playlists. No account required.

[Source](https://gitlab.com/zehkira/monophony/-/raw/master/source/data/metainfo.xml)

### Notice

This app is the successor to [Myuzi](https://linuxphoneapps.org/apps/com.gitlab.zehkira.myuzi/).
