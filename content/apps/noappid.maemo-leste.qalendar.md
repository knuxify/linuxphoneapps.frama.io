+++
title = "qalendar"
description = "Qalendar is Qt5 implementation of the Maemo Calendar"
aliases = []
date = 2020-10-16
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "maemo-leste",]
categories = [ "calendar",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtWidgets",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "Office", "Calendar",]
programming_languages = [ "Cpp",]
build_systems = [ "qmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/maemo-leste/qalendar"
homepage = "https://wiki.maemo.org/Qalendar"
bugtracker = "https://github.com/maemo-leste/qalendar/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://leste.maemo.org/Calendar"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++

### Notice

Maemo Leste has its own app store and apps are not very portable, thus candidate for removal
