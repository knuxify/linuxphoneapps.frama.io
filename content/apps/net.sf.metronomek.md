+++
title = "Metronomek"
description = "Trivial looking metronome with natural sounds and sophisticated possibilities"
aliases = []
date = 2023-10-22
updated = 2023-10-22

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "SeeLook",]
categories = []
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = []
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/SeeLook/metronomek/"
homepage = "https://metronomek.sourceforge.io"
bugtracker = "https://github.com/SeeLook/metronomek/issues"
donations = ""
translations = "https://www.opencode.net/seelook/metronomek/-/blob/master/CONTRIBUTING.md"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/SeeLook/metronomek/ef78b874686863d00ed7204f27d03f898a7a6dab/installs/Linux/net.sf.metronomek.appdata.xml"
screenshots = [ "https://a.fsdn.com/con/app/proj/metronomek/screenshots/MetronomeK-1.png", "https://a.fsdn.com/con/app/proj/metronomek/screenshots/MetronomeK-2.png", "https://a.fsdn.com/con/app/proj/metronomek/screenshots/MetronomeK-3.png", "https://metronomek.sourceforge.io/images/main.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "net.sf.metronomek"
scale_to_fit = ""
flathub = "https://flathub.org/apps/net.sf.metronomek"
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/flathub/net.sf.metronomek/master/net.sf.metronomek.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "metronomek",]
appstream_xml_url = "https://raw.githubusercontent.com/SeeLook/metronomek/ef78b874686863d00ed7204f27d03f898a7a6dab/installs/Linux/net.sf.metronomek.appdata.xml"
reported_by = "Agent Smith"
updated_by = "script"

+++

### Description

Simple metronome with natural sounds.


In spite of childish look, Metronomek application sounds quite robust with natural, pure audio sound of one among many selectable beats and rings.
 In Polish language it is a childish diminutive of a word 'metronome'.


Features:


* natural (real audio) beat sounds
* selectable sounds of ticking and ringing (i.e.: mechanical metronome, clapping, snapping, etc.)
* programmable changes of tempo (aka accelerando and rallentando)
* visible counting
* determining tempo BPM by tapping or clicking
* cross-platform: Android, Linux, Mac, Windows

[Source](https://raw.githubusercontent.com/SeeLook/metronomek/ef78b874686863d00ed7204f27d03f898a7a6dab/installs/Linux/net.sf.metronomek.appdata.xml)