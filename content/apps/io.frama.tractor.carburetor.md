+++
title = "Carburetor"
description = "The Onion Routing app"
aliases = [ "apps/org.tractor.carburetor/",]
date = 2023-03-17
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "Tractor Team",]
categories = [ "network",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "tractor",]
services = [ "Tor",]
packaged_in = [ "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "pureos_landing",]
freedesktop_categories = [ "Network", "Network",]
programming_languages = [ "Python",]
build_systems = [ "pyproject",]
requires_internet = []
tags = []

[extra]
repository = "https://framagit.org/tractor/carburetor"
homepage = "https://tractor.frama.io"
bugtracker = "https://framagit.org/tractor/carburetor/-/issues"
donations = ""
translations = "https://hosted.weblate.org/engage/carburetor"
more_information = [ "https://thisweek.gnome.org/posts/2023/01/twig-77/#third-party-projects",]
summary_source_url = "https://framagit.org/tractor/carburetor/-/raw/main/src/carburetor/desktop/io.frama.tractor.carburetor.metainfo.xml"
screenshots = [ "https://tractor.frama.io/images/carburetor-main.png", "https://tractor.frama.io/images/carburetor-preferences-general.png", "https://wiki.ubuntu.ir/images/c/cd/Carburetor-Settings.png",]
screenshots_img = [ "https://tractor.frama.io/images/carburetor-main.png", "https://tractor.frama.io/images/carburetor-preferences-general.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "io.frama.tractor.carburetor"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://framagit.org/tractor/io.frama.tractor.carburetor.flatpak/-/raw/main/io.frama.tractor.carburetor.yaml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "carburetor",]
appstream_xml_url = "https://framagit.org/tractor/carburetor/-/raw/main/src/carburetor/desktop/io.frama.tractor.carburetor.metainfo.xml"
reported_by = "danialbehzadi"
updated_by = "script"

+++

### Description
A graphical settings app for tractor which is a package uses Python stem library to provide a connection through the onion proxy and sets up proxy in user session, so you don't have to mess up with TOR on your system anymore.

[Source](https://framagit.org/tractor/carburetor/-/raw/main/src/carburetor/desktop/io.frama.tractor.carburetor.metainfo.xml)
