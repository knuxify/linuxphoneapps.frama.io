+++
title = "Contacts"
description = "Manage your contacts"
aliases = []
date = 2019-02-16
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "contacts",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_stable_23_05", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Office", "ContactManagement",]
programming_languages = [ "Vala", "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-contacts"
homepage = "https://wiki.gnome.org/Apps/Contacts"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-contacts/issues"
donations = "http://www.gnome.org/friends/"
translations = ""
more_information = [ "https://apps.gnome.org/app/org.gnome.Contacts/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-contacts/-/raw/main/data/org.gnome.Contacts.appdata.xml.in.in"
screenshots = [ "https://static.gnome.org/appdata/gnome-43/contacts/contacts-empty.png", "https://static.gnome.org/appdata/gnome-43/contacts/contacts-filled.png", "https://static.gnome.org/appdata/gnome-43/contacts/contacts-selection.png", "https://static.gnome.org/appdata/gnome-43/contacts/contacts-setup.png", "https://static.gnome.org/appdata/gnome-43/contacts/contacts-edit.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Contacts"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Contacts"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-contacts",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-contacts/-/raw/main/data/org.gnome.Contacts.appdata.xml.in.in"
reported_by = "cahfofpai"
updated_by = "script"

+++

### Description
Contacts keeps and organize your contacts information. You can create,
 edit, delete and link together pieces of information about your contacts.
 Contacts aggregates the details from all your sources providing a
 centralized place for managing your contacts.

Contacts will also integrate with online address books and automatically
 link contacts from different online sources.

[Source](https://gitlab.gnome.org/GNOME/gnome-contacts/-/raw/main/data/org.gnome.Contacts.appdata.xml.in.in)

### Notice
Used GTK3/libhandy before 42, supports .vcf import since 43.
