+++
title = "pods"
description = "Cross platform Podcast app targetting the pinephone specifically (in early development)"
aliases = []
date = 2021-05-30
updated = 2023-09-30

[taxonomies]
project_licenses = [ "No license", "all rights reserved.",]
metadata_licenses = []
app_author = [ "dskleingeld",]
categories = [ "podcast client",]
mobile_compatibility = [ "3",]
status = [ "early", "inactive",]
frameworks = [ "iced",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Network", "Audio", "Feed", "Player",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/dvdsk/pods"
homepage = ""
bugtracker = "https://github.com/dvdsk/pods/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/dvdsk/pods"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "linmob"

+++

### Notice
WIP, interface works once you get used to it. Window is not placed optimally in Phosh, hiding the title bar and showing a slice of the wallpaper.
