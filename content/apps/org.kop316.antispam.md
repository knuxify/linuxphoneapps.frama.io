+++
title = "Anti-Spam"
description = "A GTK template application"
aliases = []
date = 2021-10-25
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Chris Talbot",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "gnome-calls",]
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/kop316/phosh-antispam"
homepage = "https://gitlab.com/kop316/phosh-antispam"
bugtracker = "https://gitlab.com/kop316/phosh-antispam/issues"
donations = "https://liberapay.com/kop316/donate"
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/kop316/phosh-antispam/-/raw/master/data/metainfo/org.kop316.antispam.metainfo.xml.in"
screenshots = [ "https://gitlab.com/kop316/phosh-antispam/-/raw/master/data/metainfo/screenshot.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.kop316.antispam"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://gitlab.com/kop316/phosh-antispam/-/raw/master/org.kop316.antispam.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "phosh-antispam",]
appstream_xml_url = "https://gitlab.com/kop316/phosh-antispam/-/raw/master/data/metainfo/org.kop316.antispam.metainfo.xml.in"
reported_by = "linmob"
updated_by = "linmob"

+++

### Notice

Was GTK3/libhandy before release 3.0.

Read the README for the required version of Gnome Calls required to make this app work if you are using a stable/slow-moving distribution.
