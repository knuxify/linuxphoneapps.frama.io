+++
title = "Fractal"
description = "Matrix group messaging app"
aliases = []
date = 2019-02-01
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The Fractal Team",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "Matrix",]
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_stable_23_05", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Chat", "InstantMessaging",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/fractal"
homepage = "https://gitlab.gnome.org/GNOME/fractal"
bugtracker = "https://gitlab.gnome.org/GNOME/fractal/issues/"
donations = "https://www.gnome.org/donate/"
translations = "https://l10n.gnome.org/module/fractal/"
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/fractal"
screenshots = [ "https://gitlab.gnome.org/GNOME/fractal/raw/main/screenshots/fractal.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Fractal"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Fractal"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "fractal",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/fractal/-/raw/main/data/org.gnome.Fractal.metainfo.xml.in.in"
reported_by = "cahfofpai"
updated_by = "script"

+++

### Description
Fractal is a Matrix messaging app for GNOME written in Rust. Its interface is optimized for collaboration in large groups, such as free software projects.

[Source](https://gitlab.gnome.org/GNOME/fractal/-/raw/main/data/org.gnome.Fractal.metainfo.xml.in.in)

### Notice

Fractal 5 will be using GTK4/libadwaita for UI, and support End-to-End encryption. It's currently in beta.
