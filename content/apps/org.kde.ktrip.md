+++
title = "KTrip"
description = "Public transport navigator"
aliases = []
date = 2020-02-04
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "public transport",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = [ "KPublicTransport",]
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "arch", "archlinuxarm_aarch64", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "fedora_38", "fedora_39", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_stable_23_05", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Utility", "Utility",]
programming_languages = [ "QML", "Cpp", "Java",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/utilities/ktrip"
homepage = "https://invent.kde.org/utilities/ktrip"
bugtracker = "https://invent.kde.org/utilities/ktrip/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/utilities/ktrip/-/raw/master/org.kde.ktrip.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/ktrip/ktrip.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "org.kde.ktrip"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.ktrip"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "ktrip",]
appstream_xml_url = "https://invent.kde.org/utilities/ktrip/-/raw/master/org.kde.ktrip.appdata.xml"
reported_by = "nicolasfella"
updated_by = "script"

+++

### Description
KTrip helps you navigate in public transport. It allows you to find journeys between specified locations, departures for a specific station and shows real-time delay and disruption information.

[Source](https://invent.kde.org/utilities/ktrip/-/raw/master/org.kde.ktrip.appdata.xml)
