+++
title = "Osmin"
description = "Navigator"
aliases = []
date = 2022-01-13
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Jean-Luc Barriere",]
categories = [ "maps and navigation",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "QtQuick",]
backends = [ "libosmscout",]
services = [ "openstreetmap",]
packaged_in = [ "alpine_3_18", "alpine_edge", "aur",]
freedesktop_categories = [ "Qt", "Utility", "Maps",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/janbar/osmin"
homepage = "http://janbar.github.io/osmin/"
bugtracker = "https://github.com/janbar/osmin/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/janbar/osmin/master/osmin.appdata.xml.in"
screenshots = [ "http://janbar.github.io/osmin/download/osmin.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "osmin",]
appstream_xml_url = "https://raw.githubusercontent.com/janbar/osmin/master/osmin.appdata.xml.in"
reported_by = "Karry"
updated_by = "script"

+++

### Description
Navigator based on OSM maps

[Source](https://raw.githubusercontent.com/janbar/osmin/master/osmin.appdata.xml.in)
