+++
title = "Calls"
description = "Make phone and SIP calls"
aliases = []
date = 2019-02-01
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "GNOME Developers",]
categories = [ "telephony",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK3", "libhandy",]
backends = [ "ModemManager", "oFono", "Phonesim", "Sofia-SIP",]
services = [ "SIP",]
packaged_in = [ "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "flathub", "manjaro_stable", "manjaro_unstable", "pureos_landing",]
freedesktop_categories = [ "Audio", "Audio",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/calls"
homepage = "https://gitlab.gnome.org/GNOME/calls"
bugtracker = "https://gitlab.gnome.org/GNOME/calls/issues"
donations = ""
translations = "https://l10n.gnome.org/module/calls/"
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/calls/-/raw/main/data/org.gnome.Calls.metainfo.xml"
screenshots = [ "https://gitlab.gnome.org/GNOME/calls/raw/main/data/screenshots/calling.png", "https://gitlab.gnome.org/GNOME/calls/raw/main/data/screenshots/history.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Calls"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Calls"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-calls",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/calls/-/raw/main/data/org.gnome.Calls.metainfo.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++

### Description
Calls is a simple, elegant phone dialer and call handler for GNOME.
 It can be used with a cellular modem for plain old telephone
 calls as well as VoIP calls using the SIP protocol.

[Source](https://gitlab.gnome.org/GNOME/calls/-/raw/main/data/org.gnome.Calls.metainfo.xml)
