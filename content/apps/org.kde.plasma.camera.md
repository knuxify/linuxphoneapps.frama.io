+++
title = "Plasma Camera"
description = "Take photos and videos"
aliases = []
date = 2019-02-01
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "camera",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "manjaro_stable", "manjaro_unstable",]
freedesktop_categories = [ "Qt", "KDE", "Graphics", "Photography",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/plasma-mobile/plasma-camera"
homepage = ""
bugtracker = "https://invent.kde.org/plasma-mobile/plasma-camera/-/issues/"
donations = ""
translations = ""
more_information = [ "https://phabricator.kde.org/T6945",]
summary_source_url = "https://invent.kde.org/plasma-mobile/plasma-camera/-/raw/master/org.kde.plasma.camera.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/plasma-camera/plasma-camera.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.plasma.camera"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "plasma-camera", "kde5-plasma-camera",]
appstream_xml_url = "https://invent.kde.org/plasma-mobile/plasma-camera/-/raw/master/org.kde.plasma.camera.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++

### Description
Camera Application for Plasma Mobile.

It supports different resolutions, different white balance modes and switching between different camera devices.

[Source](https://invent.kde.org/plasma-mobile/plasma-camera/-/raw/master/org.kde.plasma.camera.appdata.xml)
