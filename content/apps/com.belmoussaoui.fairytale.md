+++
title = "Fairy Tale"
description = "Read and manage your comics collection."
aliases = []
date = 2020-10-11
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Bilal Elmoussaoui",]
categories = [ "document viewer",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Graphics", "Viewer",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/bilelmoussaoui/fairytale"
homepage = "https://gitlab.gnome.org/bilelmoussaoui/fairytale"
bugtracker = "https://gitlab.gnome.org/belmoussaoui/fairytale/issues"
donations = "https://liberapay.com/bielmoussaoui"
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/bilelmoussaoui/fairytale"
screenshots = [ "https://gitlab.gnome.org/belmoussaoui/fairytale/raw/master/data/resources/screenshots/screenshot1.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "com.belmoussaoui.FairyTale"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.gnome.org/bilelmoussaoui/fairytale/-/raw/master/data/com.belmoussaoui.FairyTale.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++

### Description
Fairy Tale is a comics/manga library manager. It ships with a minimal reader and a discovery page.

[Source](https://gitlab.gnome.org/bilelmoussaoui/fairytale/-/raw/master/data/com.belmoussaoui.FairyTale.appdata.xml.in.in)
