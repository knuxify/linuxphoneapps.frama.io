+++
title = "QiFlora"
description = "Mobile friendly application to monitor plants using Mi Flora devices."
aliases = []
date = 2019-11-08
updated = 2023-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "eyecreate",]
categories = [ "smart home",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://git.eyecreate.org/eyecreate/qiflora"
homepage = "https://git.eyecreate.org/eyecreate/qiflora"
bugtracker = "https://git.eyecreate.org/eyecreate/qiflora/-/issues/"
donations = "https://liberapay.com/eyecreate/donate"
translations = ""
more_information = []
summary_source_url = "https://git.eyecreate.org/eyecreate/qiflora/-/raw/master/packaging/org.eyecreate.qiflora.appdata.xml"
screenshots = [ "https://git.eyecreate.org/eyecreate/qiflora/raw/v1.1.2/packaging/main_window.png", "https://git.eyecreate.org/eyecreate/qiflora/raw/v1.1.2/packaging/mobile_window.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.eyecreate.qiflora"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.eyecreate.qiflora"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://git.eyecreate.org/eyecreate/qiflora/-/raw/master/packaging/org.eyecreate.qiflora.appdata.xml"
reported_by = "eyecreate"
updated_by = "script"

+++

### Description
Mobile friendly application to monitor Mi Flora devices.

Plants using this device will have their data logged when app is used and graphed to better monitor plant health.

[Source](https://git.eyecreate.org/eyecreate/qiflora/-/raw/master/packaging/org.eyecreate.qiflora.appdata.xml)
