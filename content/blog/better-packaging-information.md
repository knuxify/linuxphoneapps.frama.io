+++
title = "Better packaging information: Thank you, Repology!"
date = 2023-06-04T16:45:00+01:00
template = "blog/page.html"
draft = false 

[taxonomies]
authors = ["linmob"]

+++

Thanks to [repology.org](https://repology.org) LinuxPhoneApps.org now lists distributions that package certain apps. We came up with a script, that checks listings against repology's API. If an app is known to be available via Flathub or Snapcraft (meaning, someone manually found this and put the URL into the listing), the script also adds a tag to list these methods of app distribution as well.
<!-- more -->

We really hope this helps making LinuxPhoneApps.org more useful. Please note though, that we are not updating constantly - since most distributions are not constantly changing, we plan to run the script that queries repology monthly.

Interested in how this was done/what changed? [Check out the Merge Request that added the feature.](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/merge_requests/34)

If you've got feedback or further ideas to implement this better, make sure to get in touch!
