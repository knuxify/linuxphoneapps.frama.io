+++
title = "Welcome!"


# The homepage contents
[extra]
lead = '<b>LinuxPhoneApps.org</b> is an App Directory that lists apps for Linux Phones like the PinePhone or Librem 5, that run Linux distributions which do not have a proper (de)centralized app store yet.'
url = "apps"
url_button = "Apps"
url_games = "games"
url_button_games = "Games"
repo_license = 'Theme and Code: <a href="https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/blob/main/LICENSE">MIT</a>. Content: <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA 4.0</a>'
repo_url = "https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io"
repo_version = "beta"

[[extra.list]]
title = "For Users"
content = "Find out whether there's an app for what you would like or need to do, and how to get it!"

[[extra.list]]
title = "For Distributions and Package maintainers"
content = "Find software to package or even pre-install on your distribution."

[[extra.list]]
title = "For developers"
content = "Help developers to find efforts for similar apps, to join a project, start something similar or find links to further documentation."

[[extra.list]]
title = "Get involved"
content = 'Join our <a href="https://framagit.org/linuxphoneapps/">Framagit organization</a>, the <a href="https://matrix.to/#/#linuxphoneapps:matrix.org">Matrix Chat</a> and start coding, chatting, contributing!'

[[extra.list]]
title = "Follow along with feeds"
content = 'Get informed about project progress and added apps by following our blog by <a href="https://linuxphoneapps.org/blog/atom.xml">subscribing to its Atom feed</a> or get spammed with added apps directly by subscribing to our <a href="https://linuxphoneapps.org/apps/atom.xml">Apps feed</a> or <a href="https://linuxphoneapps.org/games/atom.xml">Games feed!</a> <br><small>If you do not use a feed reader yet, maybe <a href="https://linuxphoneapps.org/categories/feed-reader/">one of these</a> fits you well!</small>'

[[extra.list]]
title = "Follow us on Social Media!"
content = 'Get informed about project progress and added apps, or just talk to us: <a rel="me" href="https://linuxrocks.online/@linuxphoneapps">@LinuxPhoneApps@linuxrocks.online</a>. You can also follow <a href="https://twitter.com/linuxphoneapps">@LinuxPhoneApps on Xitter</a>.'
+++
