+++
title = "Debian 11"
date = 2021-08-15T08:50:45+00:00
draft = false
+++
Devuan 4.0 "Chimaera" was first released on October 14th, 2021. It's an offspring of [Debian 11 "Bullseye"](../debian-11/).
Devuan is the base distribution for [Maemo Leste](https://leste.maemo.org).

