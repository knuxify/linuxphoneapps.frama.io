#!/usr/bin/env python3

import asyncio
import datetime
import gzip
import pathlib
import re
import sys
import traceback

import aiofiles
import appstream_python
import frontmatter
import httpx
import markdownify
import utils
from lxml import etree


# NOTE Alternative: download json representation of AppStream per app: https://github.com/flathub/flathub/issues/4085
async def get_flathub_apps(client):
    flathub_catalog_url = "https://hub.flathub.org/repo/appstream/x86_64/appstream.xml.gz"
    response = await client.get(flathub_catalog_url)
    if response.status_code != httpx.codes.OK:
        raise Exception(f"Error loading {flathub_catalog_url}")

    apps_by_appstream = {}
    apps_by_flathub = {}
    components = etree.fromstring(gzip.decompress(response.content))
    for component in components.iterchildren(tag="component"):
        appstream_id = component.find("id").text
        flathub_id = component.find("bundle").text.split("/")[1]
        app = appstream_python.AppstreamComponent()
        app.parse_component_tag(component)

        app_data = {
            "app_id": appstream_id,
            "flathub_id": flathub_id,
            "appstream": app,
            "appstream_xml": component,
        }
        apps_by_appstream[appstream_id] = app_data
        apps_by_flathub[flathub_id] = app_data
    return {"by_appstream_id": apps_by_appstream, "by_flathub_id": apps_by_flathub}


def get_flathub_id_from_url(flathub_url):
    if not flathub_url:
        return None

    m = re.match(r"https://flathub.org/apps/([a-zA-Z0-9-.]+)$", flathub_url)
    if not m:
        return None

    return m.group(1)


def get_appstream_app_id(app):
    return app.id


def get_appstream_name(app):
    return app.name.get_default_text()


# possible URL types: https://www.freedesktop.org/software/appstream/docs/chap-Metadata.html#tag-url
def get_appstream_url(app, url_type):
    return app.urls.get(url_type, "") or ""  # filter out empty <url type="TYPE"></url>


def get_appstream_categories(app):
    return app.categories


def get_appstream_app_author(app):
    return app.developer_name.get_default_text().split(",")


def sanitize_licenses(licenses_str):
    return [utils.convert_to_spdx_license(license) for license in utils.multisplit(licenses_str, [" AND ", " and ", " OR ", " or "])]


def get_appstream_metadata_licenses(app):
    return sanitize_licenses(app.metadata_license)


def get_appstream_project_licenses(app):
    return sanitize_licenses(app.project_license)


def get_appstream_summary(app):
    return app.summary.get_default_text()


def get_appstream_description(app):
    return markdownify.markdownify(app.description.to_html(lang=None), heading_style="ATX")


def get_appstream_screenshots(app):
    return [screenshot.get_source_image().url.strip() for screenshot in app.screenshots if screenshot.get_source_image() is not None]


def get_appstream_display_compatible(app):
    if app.custom.get("Purism::form_factor", "") == "mobile":
        return True

    def is_display_incompatible(app, px):
        if not app.display_length:
            return True  # display_length undefined means "desktop with big screen": https://github.com/ximion/appstream/issues/481#issuecomment-1505287662

        for relation in ["requires", "recommends", "supports"]:
            props = app.display_length.get(relation, [])
            if not props:
                continue

            for prop in props:
                if not prop.compare_px(px):
                    return True

        return False

    return not is_display_incompatible(app, 360)


def get_appstream_touch_compatible(app):
    if app.custom.get("Purism::form_factor", "") == "mobile":
        return True

    def is_touch_compatible(app):
        if all(x is None for x in app.controls.values()):
            return False  # control undefined means "desktop with mouse & keyboard only": https://github.com/ximion/appstream/issues/481#issuecomment-1505287662

        if app.controls["touch"] in ["requires", "recommends", "supports"]:
            return True

        return False

    return is_touch_compatible(app)


async def check(flathub_apps: {str: {str: appstream_python.AppstreamComponent}}, item, update=False):
    item_name = utils.get_recursive(item, "extra.app_id") or utils.get_recursive(item, "title", "")
    app_id = utils.get_recursive(item, "extra.app_id")
    flathub_id = get_flathub_id_from_url(utils.get_recursive(item, "extra.flathub"))

    app_data = flathub_apps["by_flathub_id"].get(flathub_id)
    if not app_data:
        app_data = flathub_apps["by_appstream_id"].get(app_id)
    if not app_data:
        # Try fuzzy match if an app with app_id is available on Flathub
        app_data = flathub_apps["by_flathub_id"].get(app_id)
    if not app_data:
        return False
    flathub_url = f"https://flathub.org/apps/{app_data['flathub_id']}"
    app = app_data["appstream"]

    properties = [
        {"apps_key": "title", "handler": get_appstream_name},
        {"apps_key": "extra.app_id", "handler": get_appstream_app_id},
        {"apps_key": "extra.homepage", "handler": lambda app: get_appstream_url(app, "homepage")},
        {"apps_key": "extra.bugtracker", "handler": lambda app: get_appstream_url(app, "bugtracker")},
        {"apps_key": "extra.donations", "handler": lambda app: get_appstream_url(app, "donation")},
        {"apps_key": "extra.translations", "handler": lambda app: get_appstream_url(app, "translate")},
        {"apps_key": "extra.repository", "handler": lambda app: get_appstream_url(app, "vcs-browser")},
        {"apps_key": "extra.flathub", "handler": lambda app: flathub_url},
        {"apps_key": "extra.flatpak_link", "handler": lambda app: f"{flathub_url}.flatpakref"},
        {"apps_key": "taxonomies.freedesktop_categories", "handler": get_appstream_categories},
        {"apps_key": "taxonomies.app_author", "handler": get_appstream_app_author},
        {"apps_key": "taxonomies.metadata_licenses", "handler": get_appstream_metadata_licenses},
        {"apps_key": "taxonomies.project_licenses", "handler": get_appstream_project_licenses},
        {"apps_key": "description", "handler": get_appstream_summary},
        # {"apps_key": "description", "handler": get_appstream_description},
        {"apps_key": "extra.screenshots", "handler": get_appstream_screenshots},
        {"apps_key": "extra.intended_for_mobile", "handler": get_appstream_display_compatible},
        {"apps_key": "extra.all_features_touch", "handler": get_appstream_touch_compatible},
    ]
    found = False
    for property in properties:
        try:
            found_entry = utils.sanitize(property["handler"](app))
        except Exception as e:
            print(f'{item_name}: Error handling {property["apps_key"]}:', file=sys.stderr)
            traceback.print_exception(e, file=sys.stderr)
            continue

        if utils.get_recursive(item, property["apps_key"]) and not found_entry:
            print(f'{item_name}: {property["apps_key"]} missing in FlatHub AppStream file. Consider contributing it upstream: {utils.get_recursive(item, property["apps_key"])}', file=sys.stderr)
        if not found_entry or found_entry == utils.get_recursive(item, property["apps_key"]):
            continue  # already up to date

        message = f'{item_name}: {property["apps_key"]} '
        if not utils.get_recursive(item, property["apps_key"]):
            message += "new: "
        else:
            message += f'outdated "{utils.get_recursive(item, property["apps_key"])}" -> '
        message += f'"{found_entry}"'
        print(message, file=sys.stderr)

        found = True
        if update:
            utils.set_recursive(item, property["apps_key"], found_entry)

            source_column = property["apps_key"] + "_source"
            if property["apps_key"] == "description":
                source_column = "extra.summary_source_url"
            if utils.isset_recursive(item, source_column):
                if utils.get_recursive(item, source_column) != flathub_url:
                    print(f"{item_name}: {source_column} {utils.get_recursive(item, source_column)} -> {flathub_url}", file=sys.stderr)
                utils.set_recursive(item, source_column, flathub_url)

            utils.set_recursive(item, "updated", datetime.date.today())
            utils.set_recursive(item, "extra.updated_by", "script")

    return found


async def new(folder: pathlib.Path, flathub: str):
    if not flathub.startswith("http"):
        flathub = f"https://flathub.org/apps/{flathub}"

    async with httpx.AsyncClient(timeout=30.0) as client:
        flathub_apps = await get_flathub_apps(client)

        doc = frontmatter.Post(content="")
        doc.metadata = {"extra": {"flathub": flathub}}
        found = await check(flathub_apps, doc.metadata, True)

        if found:
            utils.set_recursive(doc.metadata, "date", datetime.date.today())
            utils.set_recursive(doc.metadata, "extra.reported_by", "script")

            folder.mkdir(parents=True, exist_ok=True)
            filename = folder / utils.generate_filename(doc.metadata)
            print(f"Writing changes to {filename}")
            async with aiofiles.open(filename, mode="w", encoding="utf-8") as f:
                await f.write(frontmatter.dumps(doc, handler=frontmatter.default_handlers.TOMLHandler()))

        return found


async def check_file(flathub_apps: {str: {str: appstream_python.AppstreamComponent}}, filename: pathlib.Path, update=False):
    async with aiofiles.open(filename, mode="r", encoding="utf-8") as f:
        doc = frontmatter.loads(await f.read())

    found = await check(flathub_apps, doc.metadata, update)

    if found and update:
        print(f"Writing changes to {filename}")
        async with aiofiles.open(filename, mode="w", encoding="utf-8") as f:
            await f.write(frontmatter.dumps(doc, handler=frontmatter.default_handlers.TOMLHandler()))

    return found


async def run(path: pathlib.Path, update: bool = False):
    async with httpx.AsyncClient(timeout=30.0) as client:
        flathub_apps = await get_flathub_apps(client)

        tasks = []
        files = path.glob("**/*.md") if path.is_dir() else [path]
        for filename in files:
            if filename.name == "_index.md":
                continue
            tasks.append(asyncio.ensure_future(check_file(flathub_apps, filename, update)))
        found = any(await asyncio.gather(*tasks))
        return found


async def main():
    if len(sys.argv) < 3:
        print(f"Syntax:\n {sys.argv[0]} check|fix FOLDER\n {sys.argv[0]} new FOLDER FLATHUB_ID|FLATHUB_URL", file=sys.stderr)
        sys.exit(1)

    mode = sys.argv[1]
    apps_folder = pathlib.Path(sys.argv[2])

    if mode == "new":
        flathub_id = sys.argv[3]
        found = await new(apps_folder, flathub_id)
        if not found:
            print(f"Error importing app {flathub_id}.", file=sys.stderr)
            sys.exit(1)
    else:
        update = mode == "fix"
        found = await run(apps_folder, update)
        if found and not update:
            print(f'Errors found! Run "{sys.argv[0]} fix {apps_folder}" to apply suggested changes.', file=sys.stderr)
            sys.exit(1)


if __name__ == "__main__":
    asyncio.run(main())
