
# Checkers

This folder contains scripts that help with checking / fixing outdated content, broken links and more.

They can be triggered from https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/ci/editor?tab=1 or run locally on your machine.

## Local install

To run them locally, first clone the repository and cd to this folder:

~~~~
git clone --recurse-submodules https://github.com/linuxphoneapps/linuxphoneapps.org.git
cd linuxphoneapps.frama.io/checkers
~~~~

Now, you should install the necessary dependencies

~~~~
python3 -m pip install -r requirements.txt
~~~~

You can now run the checkers on _all files in a folder_ or _on just one file_ (example with check_via_appstream.py, other checkers work the same way): 

~~~~
python3 check_via_appstream.py check $folder/($file.md)
~~~~
if you just want to check a listing, or 
~~~~
python3 check_via_appstream.py fix $folder/($file.md)
~~~~ 

if you want to attempt fixing outdated information automatically.

## Additional Use Cases


Aside from checking existing listings, you can also use the checkers to save time when creating new listings - `check_via_appstream.py` and `check_via_repology.py` are especially helpful for this. 

You also start generating a new listing via 

~~~~
python3 check_via_appstream.py new PATH APPSTREAM_XML_URL
~~~~

with APPSTREAM_XML_URL being the raw variant of an AppStream XML metadata file, typically containing the word metainfo or appdata in its filename and being located in the root folder of a project or in its data subfolder. 

The PATH is typically ../content/apps for a new app or ../content/games for a new game.

Note that the generated file will have an incorrect filename if autotools variables are used in the projects file for the app id (<id></id>) and it may not be complete - one good way to check this is (assuming you have cloned the linuxphoneapps.frama.io project to run the checkers) is to run `zola serve` to check that the site still builds with the new listing.

It is recommended to comment out lines 185 and 186 in https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/blob/main/checkers/check_via_appstream.py?ref_type=heads#L185-186 before running the checkers in `fix` mode on new listings. 

