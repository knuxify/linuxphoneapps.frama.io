#!/usr/bin/env python3

import asyncio
import os
import pathlib
import re
import sys

import aiofiles
import frontmatter
import httpx

import utils


async def check_file(client: httpx.AsyncClient, filename: pathlib.Path, update: bool = False):
    async with aiofiles.open(filename, mode="r", encoding="utf-8") as f:
        doc = frontmatter.loads(await f.read())

    generated_filename = filename.parent / utils.generate_filename(doc.metadata)
    if filename == generated_filename:
        return False

    print(f"File name should be {generated_filename} instead of {filename}", file=sys.stderr)
    if update:
        print(f"Renaming {filename} to {generated_filename}")
        os.rename(filename, generated_filename)

    return True


async def run(path: pathlib.Path, update: bool = False):
    async with httpx.AsyncClient(timeout=30.0) as client:
        tasks = []
        files = path.glob("**/*.md") if path.is_dir() else [path]
        for filename in files:
            if filename.name == "_index.md":
                continue
            tasks.append(asyncio.ensure_future(check_file(client, filename, update)))
        found = any(await asyncio.gather(*tasks))
        return found


async def main():
    if len(sys.argv) < 3:
        print(f"Syntax: {sys.argv[0]} check|fix PATH")
        sys.exit(1)
    update = sys.argv[1] == "fix"
    apps_path = pathlib.Path(sys.argv[2])
    found = await run(apps_path, update)
    if found and not update:
        print(f'Errors found! Run "{sys.argv[0]} fix {apps_path}" to apply suggested changes.', file=sys.stderr)
        sys.exit(1)


if __name__ == "__main__":
    asyncio.run(main())
